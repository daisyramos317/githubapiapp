//
//  RepoViewModel.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import Foundation


class RepoViewModel: RepoViewModelProtocol, AddIssueViewModelProtocol {
    
    var username: String!
    var repoFullName: String!
    var searchTerm: String!
    
    var title: String!
    var body: String!
    
    var repoUpdated = false
    
    weak var delegate: RepoDelegate?
    
    
    func downloadRepositories() {
        GitHubProvider.request(.Repos(username: username)) { result in
            do {
                let response = try result.dematerialize()
                let value = try response.mapNSArray()
                
                
                let repos = Repository.from(value)!
            
                RepoManager.shared.addRepo(array: repos)
                
                self.delegate?.fetched(success: true, error: "")
                
            } catch {
                self.delegate?.fetched(success: false, error: error.localizedDescription)
            }
        }
    }
    
    func downloadNewRepositories() {
        GitHubProvider.request(.Repos(username: username)) { result in
            do {
                let response = try result.dematerialize()
                let value = try response.mapNSArray()
                
                let ownerValue = value.value(forKey: "owner")
                let owners = Owner.from(ownerValue as! NSArray)
                
                let repos = Repository.from(value)!
              
                
                if (repos.count == 0
                    && owners?.count == 0){
                    self.delegate?.fetched(success: false, error: "This repo is empty, try another one?")
                } else {
                    RepoManager.shared.repos.removeAll()
                    RepoManager.shared.owners.removeAll()
                    
                    RepoManager.shared.addOwner(array: owners!)
                    RepoManager.shared.addRepo(array: repos)
                    
                    print("New Repo from manager \(RepoManager.shared.repos!)")
                    
                    self.repoUpdated = true
                    self.delegate?.fetched(success: true, error: "")
                }
                
            } catch {
                self.repoUpdated = false
                self.delegate?.fetched(success: false, error: ("\(error.localizedDescription) Or person does not exist 🤔"))
            }
        }
    }
    
    
    func downloadNewRepoWithSearchTerm() {
        GitHubProvider.request(.Repo(fullName: searchTerm)) { result in
            do {
                let response = try result.dematerialize()
                let value = try response.mapNSDictionary()
                
                
                let dictJson = value.value(forKey: "items")
                let dict = dictJson as? NSArray
                let repos = Repository.from(dictJson as! NSArray)
                
                let owners = dict?.value(forKey: "owner")
                let ownerArr = Owner.from(owners as! NSArray)
                
                
                if (repos?.count == 0 && ownerArr?.count == 0){
                    self.delegate?.fetched(success: false, error: "This repo is empty, try another one?")
                } else {
                    RepoManager.shared.repos.removeAll()
                    RepoManager.shared.owners.removeAll()
                    
                    RepoManager.shared.addOwner(array: ownerArr!)
                    RepoManager.shared.addRepo(array: repos!)
                    
                    print("New Repo from manager \(RepoManager.shared.repos!)")
                    
                    self.repoUpdated = true
                    self.delegate?.fetched(success: true, error: "")
                }
                
            } catch {
                self.repoUpdated = false
                self.delegate?.fetched(success: false, error: ("\(error.localizedDescription) Or person does not exist 🤔"))
            }
        }
    }
    
    func downloadCommitsWithCompletionHandler(completionHandler : ((_ error : Error?) -> Void)?) {
        
        GitHubProvider.request(.Commits(repoFullName: repoFullName)) { result in
            do{
                
                let response = try result.dematerialize()
                let value = try response.mapNSArray()
                
                let commitAuthor = value.value(forKey: "commit")
                let commits = Commit.from(commitAuthor as! NSArray)
                RepoManager.shared.commits.removeAll()
                RepoManager.shared.addCommit(commit: commits!)
                
               
                    print("Commits fetched: \(value.count)")
                    
                
                
                completionHandler?(nil)
            } catch {
                
                completionHandler?(error)
            }
        }
    }
    
    func downloadIssuesWithCompletionHandler(completionHandler : ((_ issueError : Error?) -> Void)?) {
        
        GitHubProvider.request(.Issues(repoFullName: repoFullName)) { result in
            do{
                
                let response = try result.dematerialize()
                let value = try response.mapNSArray()
                
                
                let issues = Issue.from(value)!
                RepoManager.shared.issues.removeAll()
                RepoManager.shared.addIssue(issue: issues)
               
                    print("Issues fetched: \(value.count)")
                    
                
                completionHandler?(nil)
            } catch {
                
                completionHandler?(error)
            }
        }
    }
    
    func downloadPullsWithCompletionHandler(completionHandler : ((_ pullError : Error?) -> Void)?) {
        
        GitHubProvider.request(.Pulls(repoFullName: repoFullName)) { result in
            do{
                
                let response = try result.dematerialize()
                let value = try response.mapNSArray()
                
                
                let pulls = PullRequest.from(value)!
                RepoManager.shared.pulls.removeAll()
                RepoManager.shared.addPull(pull: pulls)
              
                    print("Pulls fetched: \(value.count)")
                    
                
                completionHandler?(nil)
            } catch {
                
                completionHandler?(error)
            }
        }
    }

    
    
}
