//
//  PullRequest.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import Mapper

struct PullRequest: Mappable {
    
    let title: String
    let dateCreated: String
    let state: String
    let number: Int
    
    
    init(map: Mapper) throws {
        try title = map.from("title")
        try dateCreated = map.from("created_at")
        try state = map.from("state")
        try number = map.from("number")
        
    }
}
