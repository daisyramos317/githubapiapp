//
//  Repository.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//
import Mapper


struct Repository: Mappable {
    let identifier: Int
    let name: String
    let fullRepoName: String
    
    
    
    init(map: Mapper) throws {
        try identifier = map.from("id")
        try name = map.from("name")
        try fullRepoName = map.from("full_name")
     
        
    }
    
}
