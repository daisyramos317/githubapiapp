//
//  RepoCell.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit
import SDWebImage

class RepoCell: UITableViewCell {

    static let Identifier = "RepoCell"
    
    
    @IBOutlet weak var ownerImage: UIImageView!
    @IBOutlet weak var repoTitle: UILabel!
    
    func configureWithRepo(repo: Repository, owner: Owner){
        self.repoTitle.adjustsFontSizeToFitWidth = true
        self.repoTitle.text = repo.name
        self.ownerImage.asCircle()
        self.ownerImage.sd_setImage(with: URL(string: owner.avatarUrl))
        
    }

}
