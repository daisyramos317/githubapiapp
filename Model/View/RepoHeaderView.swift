//
//  RepoHeaderView.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit

class RepoHeaderView: UIView{

    @IBOutlet weak var imageView: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
