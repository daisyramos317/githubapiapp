//
//  SearchReposViewController.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit
import MBProgressHUD

class SearchReposViewController: UIViewController, UISearchResultsUpdating, UISearchBarDelegate, RepoDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    static var repoTableViewCellIdentifier: String = "RepoCell"

    var repos: [Repository]? = []
    var owners: [Owner]? = []
    
    
    var viewModel = RepoViewModel()
    
    
    let searchController = UISearchController(searchResultsController: nil)
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: RepoFetchFinishedNotification), object: self)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: RepoFetchFailedNotification), object: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        self.showLoader()
        NotificationCenter.default.addObserver(self, selector: #selector(SearchReposViewController.repoFetchCompleted(notification:)), name: NSNotification.Name(rawValue: RepoFetchFinishedNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SearchReposViewController.repoFetchFailed(notification:)), name: NSNotification.Name(rawValue: RepoFetchFailedNotification), object: nil)
        
        self.navigationController?.navigationBar.isTranslucent = true
        
        self.extendedLayoutIncludesOpaqueBars = true
        self.searchController.searchResultsUpdater = self
    
        self.tableView.tableHeaderView = self.searchController.searchBar
        self.searchController.searchBar.scopeButtonTitles = ["All", "Username"]
        self.searchController.searchBar.delegate = self
        self.tableView.tableHeaderView?.clipsToBounds = true
        self.searchController.searchBar.sizeToFit()
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.definesPresentationContext = true
    }
    
    func showLoader(){
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
    }
    
  
    func repoFetchCompleted(notification: Notification){
        
        if (repos != nil){
            if (repos?.count)! < 1 {
                self.repos = RepoManager.shared.repos
                self.owners = RepoManager.shared.owners
               
                DispatchQueue.main.async {
                   
                    self.tableView.reloadData()
                }
            }
        }
        
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func repoFetchFailed(notification: Notification){
        MBProgressHUD.hide(for: self.view, animated: true)
        self.displayAlertMessage(message: "Fetching repos failed please check your internet connection and try again")

    }
    
    func fetched(success: Bool, error: String) {
        if success {
            
            if viewModel.repoUpdated == true {
                
                self.repos?.removeAll()
                self.owners?.removeAll()
                //apparently performance on removeAll() for collections is not so fast
                sleep(1)
                if (repos != nil){
                    if (repos?.count)! < 1 {
                        
                        
                        self.searchController.isActive = false
                        self.owners = RepoManager.shared.owners
                        self.repos = RepoManager.shared.repos
                        
                    }
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
            
            
            
        } else {
            self.searchController.isActive = false
            self.displayAlertMessage(message: error)
        }
    }
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        if (scope == self.searchController.searchBar.scopeButtonTitles![0]) {
            
            viewModel.searchTerm = searchText.lowercased()
        }
        else if (scope == self.searchController.searchBar.scopeButtonTitles![1]) { //Username
            viewModel.username = searchText.lowercased()
        }
        
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        if (scope == self.searchController.searchBar.scopeButtonTitles![0]) {
            viewModel.downloadNewRepoWithSearchTerm()
        }
        else if (scope == self.searchController.searchBar.scopeButtonTitles![1]) { //Username
            viewModel.downloadNewRepositories()
        }
        
    }
    
    
    //MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        self.filterContentForSearchText(searchText: searchController.searchBar.text!, scope: scope)
    }
    
    //MARK: UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchText: searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
    
    
    
    
}

extension SearchReposViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if repos != nil {
            return repos!.count
        }
        return 0
        
    }
}

extension SearchReposViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        guard let cell = tableView.dequeueReusableCell(withIdentifier: RepoCell.Identifier, for: indexPath) as? RepoCell else {
            //Something went wrong with the identifier.
            return UITableViewCell()
        }
        
        
        let repo = repos?[indexPath.row]
        let owner = owners?[indexPath.row]
        
        cell.configureWithRepo(repo: repo!, owner: owner!)
              return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let repo = repos?[indexPath.row]
        let owner = owners?[indexPath.row]
        
        self.searchController.isActive = false
        let detailVC: DetailViewController = DetailViewController()
        detailVC.repo = repo
        detailVC.owner = owner
        detailVC.title = repo?.name
        
        viewModel.username = owner?.loginName
        viewModel.repoFullName = repo?.fullRepoName
        viewModel.downloadCommitsWithCompletionHandler(completionHandler: {(error: Swift.Error?) -> Void in
            self.viewModel.downloadIssuesWithCompletionHandler(completionHandler: {(issueError: Swift.Error?) -> Void in
                self.viewModel.downloadPullsWithCompletionHandler(completionHandler: {(pullError: Swift.Error?) -> Void in
                    if ((error == nil) && (issueError == nil) && (pullError == nil)) {
                        self.navigationController?.pushViewController(detailVC, animated: true)
                    } else {
                        self.displayAlertMessage(message: "Something went wrong when fetching data please try again")
                    }
                })
            })
        })
        
    }
}
