//
//  PullRequestsViewController.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit

class PullRequestsViewController: UIViewController {

    var pulls = [PullRequest]()
    static var pullRequestTableViewCellIdentifier: String = "PullRequestCell"
    
    @IBOutlet weak var tableView: UITableView!
    var messageLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let rect = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height)
        messageLabel = UILabel(frame: rect)
        
        
        messageLabel.text = "🎉 This repository has no pull requests, check back later"
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        
        if RepoManager.shared.pulls.count != 0 {
            self.pulls = RepoManager.shared.pulls
            
            DispatchQueue.main.async {
                self.messageLabel.isHidden = true
                self.tableView.reloadData()
            }
            
        } else {
            self.tableView.backgroundView = messageLabel
            self.tableView.separatorStyle = .none
        }
      
    }
    

}

extension PullRequestsViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return pulls.count
        
    }
}

extension PullRequestsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier: String = IssuesViewController.issueTableViewCellIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)
            ?? UITableViewCell(style: .subtitle, reuseIdentifier: reuseIdentifier)
        let pull = pulls[indexPath.row]
        
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.long
        formatter.timeStyle = .medium
        
        let date = dateFromString(dateString: pull.dateCreated)
        let dateString = formatter.string(from: date!)
        
        
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.text = "\(pull.title) #\(pull.number)"
        cell.detailTextLabel?.textColor = UIColor.blue
        cell.detailTextLabel?.text = "\(dateString)"
        return cell
        
    }
}

