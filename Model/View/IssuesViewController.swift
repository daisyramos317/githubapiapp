//
//  IssuesViewController.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit
import Foundation
import Moya

class IssuesViewController: UIViewController {

    var issues = [Issue]()
    var repo: Repository!
    var owner: Owner!
    
    static var issueTableViewCellIdentifier: String = "IssueCell"
    
    @IBOutlet weak var tableView: UITableView!
    var messageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    
        
        let rect = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height)
        messageLabel = UILabel(frame: rect)
        
        
        messageLabel.text = "This repository has no issues, make a new one? 😏"
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        
        if RepoManager.shared.issues.count != 0 {
            self.issues = RepoManager.shared.issues
            
            DispatchQueue.main.async {
                self.messageLabel.isHidden = true
                self.tableView.reloadData()
            }
        } else {
            self.tableView.backgroundView = messageLabel
            self.tableView.separatorStyle = .none
        }
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func addButtonTapped(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Add Issue", message: "Please add your issue below", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Submit", style: .default) { (_) in
            let titleField = alertController.textFields![0]
            let bodyField = alertController.textFields![1]
            
            
            if (!(titleField.text ?? "").isEmpty || !(bodyField.text ?? "").isEmpty){
                

                
                self.displayAlertMessage(message: "This feature requires authentication :(")
                
            }
            else{
                self.displayAlertMessage(message: "Fields cannot be empty")
            }
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Title"
        }
        alertController.addTextField { textField in
            textField.placeholder = "Description"
            
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
   

}

extension IssuesViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return issues.count
        
    }
}

extension IssuesViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier: String = IssuesViewController.issueTableViewCellIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)
            ?? UITableViewCell(style: .value2, reuseIdentifier: reuseIdentifier)
        let issue = issues[indexPath.row]
        
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.text = issue.title
        cell.detailTextLabel?.textColor = UIColor.blue
        cell.detailTextLabel?.text = "#\(issue.number)"
        return cell
        
    }
}
