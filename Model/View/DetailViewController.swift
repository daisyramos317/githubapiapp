//
//  DetailViewController.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit
import Eureka

class DetailViewController: FormViewController {
    public var repo: Repository!
    public var owner: Owner!
    public var commit: Commit!
    
    var animateHeader: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        form +++ Section() {
            var header = HeaderFooterView<RepoHeaderView>(.nibFile(name: "RepoHeaderView", bundle: nil))
            header.onSetupView = { (view, section) -> () in
                if (self.animateHeader) {
                    let avatarURL: URL = RepoManager.avatarUrlForRepo(owner: self.owner, view: view)
                    view.imageView.sd_setImage(with: avatarURL)
                    view.imageView.alpha = 0;
                    UIView.animate(withDuration: 2.0, animations: { [weak view] in
                        view?.imageView.alpha = 1
                    })
                    view.layer.transform = CATransform3DMakeScale(0.9, 0.9, 1)
                    UIView.animate(withDuration: 1.0, animations: { [weak view] in
                        view?.layer.transform = CATransform3DIdentity
                    })
                }
            }
            $0.header = header
            }
            +++ Section("Commits"){_ in }
            <<< TextAreaRow(){
                $0.value = "\(String(RepoManager.shared.commits.count))"
                $0.disabled = true
                $0.textAreaHeight = .dynamic(initialTextViewHeight: 10)
            }
            
            +++ Section(){_ in }
            <<< ButtonRow(){
                $0.title = "Issues: \(RepoManager.shared.issues.count)"
                $0.presentationMode = PresentationMode.show(
                    controllerProvider: ControllerProvider.callback {
                        let issuesVC: IssuesViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "issuesVC") as! IssuesViewController
                        issuesVC.issues = RepoManager.shared.issues
                        issuesVC.repo = self.repo
                        return issuesVC },
                    onDismiss: { vc in }
                )}
            +++ Section(){_ in }
            <<< ButtonRow(){
                $0.title = "Pull Requests: \(RepoManager.shared.pulls.count)"
                $0.presentationMode = PresentationMode.show(
                    controllerProvider: ControllerProvider.callback {
                        let pullsVC: PullRequestsViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pullsVC") as! PullRequestsViewController
                        pullsVC.pulls = RepoManager.shared.pulls
                        return pullsVC },
                    onDismiss: { vc in }
                )}
        
        
        
    }

    
}
