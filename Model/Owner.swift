//
//  Owner.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import Mapper


struct Owner: Mappable{
    let loginName: String
    let avatarUrl: String
    
    init(map: Mapper) throws {
        try loginName = map.from("login")
        try avatarUrl = map.from("avatar_url")
    }
    
}

