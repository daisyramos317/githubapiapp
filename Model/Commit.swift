//
//  Commit.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import Mapper

struct Commit: Mappable {
    
    let message: String
    
    init(map: Mapper) throws {
        try message = map.from("message")
    }
    
}
