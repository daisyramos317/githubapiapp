//
//  Protocols.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import Foundation

import Foundation


protocol RepoDelegate: class {
    func fetched(success : Bool, error : String)
    
}

protocol RepoViewModelProtocol: class {
    
    var username: String! { get }
    var repoFullName: String! { get }
    var searchTerm: String! { get }
    
    
}

protocol AddIssueViewModelProtocol: class {
    
    var title: String! { get }
    var body: String! { get }
}
