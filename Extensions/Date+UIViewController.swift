//
//  Date+UIViewController.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func dateFromString(dateString :String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        
        let date = dateFormatter.date(from: dateString)
        
        return date
    }
    
}
