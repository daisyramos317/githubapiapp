//
//  UIImageView.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit

extension UIImageView {
    func asCircle(){
        
        self.layer.cornerRadius = self.frame.width / 2;
        self.layer.borderColor = UIColor.clear.cgColor
        self.clipsToBounds = true
        self.layer.masksToBounds = true
    }
}
