# README #

###GitHubAPIApp ###

The purpose of this application is to demonstrate functionality of the GitHub API with the ability to search repositories (by topic name and username), display commit count, issues, and pull requests.

Technical Decisions


      -Following a custom MVVM pattern composed of Models, Views, ViewModels, Services for networking/validation and Extenstions for UI binding.
      -This app also displays different methods for passing data using Delegates, Notifications and Callbacks.
      -GithubAPIService is responsible for all network calls and mapping the json data.
      -I also used Eureka which is a nice form builder for Swift the syntax is a bit different but readable 

Note: In order to post an issue to a public repository the user needs to authenticate themselves, I skipped this one and in place added the ability to search for any repository. Also pod install might be necessary when building for the first time.


