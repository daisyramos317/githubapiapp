//
//  RepoManager.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import Foundation
import Moya

class RepoManager: NSObject {
    
    static let shared = RepoManager()
    
    
    var repos: [Repository]!
    var commits: [Commit]!
    var owners: [Owner]!
    var issues: [Issue]!
    var pulls: [PullRequest]!
    
    private override init(){
        self.repos = [Repository]()
        self.owners = [Owner]()
        self.commits = [Commit]()
        self.issues = [Issue]()
        self.pulls = [PullRequest]()
        super.init()
    }
    
    public func addPull(pull: [PullRequest]){
        self.pulls.append(contentsOf: pull)
    }
    
    public func addIssue(issue: [Issue]){
        self.issues.append(contentsOf: issue)
    }
    
    public func addCommit(commit: [Commit]){
        self.commits.append(contentsOf: commit)
    }
    
    public func addOwner(array: [Owner]){
        self.owners.append(contentsOf: array)
    }
    
    public func addRepo(array: [Repository]){
        
        self.repos.append(contentsOf: array)
    }
    
    public static func avatarUrlForRepo(owner: Owner, view: UIView) -> URL {
        
        let imagePath: String = owner.avatarUrl
        return URL(string: imagePath)!
    }



}
