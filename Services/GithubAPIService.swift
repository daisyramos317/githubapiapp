//
//  GithubAPIService.swift
//  GitHubAPIApp
//
//  Created by Daisy Ramos on 6/9/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import Foundation
import Moya


let RepoFetchFinishedNotification: String = "RepoFetchFinishedNotification"
let RepoFetchFailedNotification: String = "RepoFetchFailedNotification"


// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data
    }
}

let GitHubProvider = MoyaProvider<GitHub>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])

enum GitHub {
    case Repos(username: String)
    case Repo(fullName: String)
    case Issues(repoFullName: String)
    case Commits(repoFullName: String)
    case Pulls(repoFullName: String)
   
}

private extension String {
    var URLEscapedString: String {
        return addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
}

extension GitHub: TargetType {
    public var baseURL: URL { return URL(string: "https://api.github.com")! }
    
    
    public var path: String {
        switch self {
        case .Repos(let name): return "/users/\(name.URLEscapedString)/repos"
        case .Repo( _): return "/search/repositories"
        case .Issues(let repoFullName): return "/repos/\(repoFullName)/issues"
        case .Commits(let repoFullName): return "/repos/\(repoFullName)/commits"
        case .Pulls(let repoFullName): return "/repos/\(repoFullName)/pulls"
        
        }
    }
    
    public var method: Moya.Method {
      
            return .get
        
    }
    
    public var parameters: [String:Any]? {
        switch self {
        
        case .Repo(let searchTerm):
            return ["q": searchTerm, "sort": "stars", "per_page": "10"]
            
        case .Issues(_):
            return ["state": "open"]
            
        default:
            return nil
        }
    }
    
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    public var task: Task {
        return .request
    }
    
    //Required to conform to TargetType protocol
    public var sampleData: Data {
        switch self {
        case .Repos(_): return "[{\"name\": \"Repo Name\"}]".data(using: String.Encoding.utf8)!
            
        case .Repo(_): return "{\"id\": \"1\", \"language\": \"Swift\", \"url\": \"https://api.github.com/repos/moya/Router\", \"name\": \"Router\"}".data(using: String.Encoding.utf8)!
            
        case .Issues(_): return "{\"id\": 132942471, \"number\": 405, \"title\": \"}".data(using: String.Encoding.utf8)!
            
        case .Commits(_): return "".data(using: String.Encoding.utf8)!
            
        case .Pulls(_): return "".data(using: String.Encoding.utf8)!
         
        }
    }
}

public func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}

//MARK: - Response Handlers

extension Moya.Response {
    func mapNSArray() throws -> NSArray {
        let any = try self.mapJSON()
        guard let array = any as? NSArray else {
            throw MoyaError.jsonMapping(self)
        }
        return array
    }
    func mapNSDictionary() throws -> NSDictionary {
        let any = try self.mapJSON()
        guard let dict = any as? NSDictionary else {
            throw MoyaError.jsonMapping(self)
        }
        return dict
    }
    
}
